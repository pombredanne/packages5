Name:           doomsday
Version:        1.13.2
Release:        2%{?dist}
Summary:        DOOM/Hertic/Hexen port with pretty graphics
License:        GPLv3+
URL:            http://dengine.net
Source0:        http://dengine.net/files/%{name}-%{version}.tar.gz

BuildRequires:  freealut-devel
BuildRequires:  freeglut-devel
BuildRequires:  SDL-devel
BuildRequires:  SDL_mixer-devel
BuildRequires:  libpng-devel
BuildRequires:  ncurses-devel
BuildRequires:  cmake
BuildRequires:  curl-devel
BuildRequires:  xz
BuildRequires:  zlib-devel
BuildRequires:  qt-devel
Requires:       timidity++

%description
The Doomsday Engine is a greatly enhanced DOOM source port available for
multiple platforms.

Doomsday supports many games including the classic first-person shooters DOOM,
Heretic and Hexen.

%package launcher
Summary:        Graphical launcher for Doomsday
Requires:       %name = %version
BuildRequires:  python2
Requires:       wxPython
BuildArch:      noarch

%description launcher
Snowberry is the official frontend of the Doomsday Engine. It is a portable
graphical game launcher, with support for configuring the various types of
games supported by Doomsday. It makes it easy to save your settings for
launching a particular game (e.g., Doom), so you don't have to type in a
lengthy command line every time you start a game.

%prep
%setup -q

%build
%qmake_qt4 -r doomsday/doomsday.pro
make -f doomsday/Makefile %{?_smp_mflags}

%install
make -f doomsday/Makefile install INSTALL_ROOT=$RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%{_bindir}/*
%{_libdir}/doomsday/
%{_libdir}/lib*.so
%{_libdir}/lib*.so.*
%{_datadir}/applications/*
%{_datadir}/doomsday/
%{_mandir}/man6/*

%files launcher
%{_bindir}/launch-doomsday
%{_datadir}/%name/snowberry

%changelog
* Thu Mar 13 2014 Dhiru Kholia <dhiru@openwall.com> - 1.13.2-2
- misc. fixes (BZ #1071062)

* Thu Feb 27 2014 Dhiru Kholia <dhiru@openwall.com> - 1.13.2-1
- initial version (based on .spec written by Juan Manuel Borges Caño)
