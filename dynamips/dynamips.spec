# http://svnweb.mageia.org/packages/cauldron/dynamips/current/SPECS/dynamips.spec

Name:           dynamips
Version:        0.2.12
Release:        1%{?dist}
Summary:        Cisco router Emulator

BuildRequires:  elfutils-libelf-devel
BuildRequires:  libpcap-devel
BuildRequires:  libuuid-devel
BuildRequires:  glibc-devel
URL:            http://www.gns3.net
# https://github.com/GNS3/dynamips
Source:         https://github.com/GNS3/dynamips/archive/v0.2.11.tar.gz#/%{name}-%{version}.tar.gz
License:        GPLv2
ExclusiveArch:  %{ix86} x86_64

%description
Dynamips is a software that emulates Cisco IOS on a traditional PC. It has been
created by Christophe Fillot who started his work in August 2005. The last
official release of Dynamips supports Cisco 7200, 3600 series (3620, 3640 and
3660), 3700 series (3725, 3745), 2600 series (2610 to 2650XM, 2691) and 1700
series. It is very popular with people studying for CCNA (Cisco Certified
Network Associate), CCNP (Cisco Certified Network Professional) and CCIE (Cisco
Certified Internetwork Expert) certification exams. Please note that you must
provide your own IOS images compatible with the Cisco hardware emulated by
Dynamips.

This is Cisco Router Emulator developed by GNS3 Community.

%prep
%setup -q

%build
CFLAGS="%{optflags}"
CXXFLAGS="%{optflags}"
LDFLAGS="%{?__global_ldflags}"
export CFLAGS
export CXXFLAGS

%ifarch x86_64
make DYNAMIPS_ARCH=amd64 %{?_smp_mflags}
%else
make %{?_smp_mflags}
%endif

%install

%ifarch x86_64
make DYNAMIPS_ARCH=amd64 install DESTDIR=%{buildroot} mandir=%{_mandir} bindir=%{_bindir} prefix=%{_prefix}
%else
make install DESTDIR=%{buildroot} mandir=%{_mandir} bindir=%{_bindir} prefix=%{_prefix}
%endif
sed -i 's/\r$//' COPYING README README.hypervisor

%files
%doc COPYING README README.hypervisor
%{_bindir}/dynamips
%{_bindir}/nvram_export
%{_mandir}/man?/*

%changelog
* Fri Feb 28 2014 Dhiru Kholia <dhiru@openwall.com> - 0.2.12-1
- BZ #1071109 fixes, package latest version from GitHub
