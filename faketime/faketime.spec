Name:        faketime
Version:     0.9.5
Release:     1%{dist}
Summary:     Report faked system time to programs
License:     GPLv2

URL:         http://www.code-wizards.com/projects/libfaketime/
Source0:     http://www.code-wizards.com/projects/libfaketime/libfaketime-%{version}.tar.gz

Patch0:      faketime-fix-infinite-recursion-on-real_clock_gettime.patch

%description
libfaketime intercepts various system calls which programs use to retrieve the
current date and time. It can then report faked dates and times (as specified
by you, the user) to these programs. This means you can modify the system time
a program sees without having to change the time system-wide.

%prep
%setup -q -n libfaketime-%{version}
# work around from upstream for autodetecting glibc version bug on i686 (Paul Wouters)
sed -i -e 's/__asm__(".symver timer_gettime_22/\/\/__asm__(".symver timer_gettime_22/' src/libfaketime.c
sed -i -e 's/__asm__(".symver timer_settime_22/\/\/__asm__(".symver timer_settime_22/' src/libfaketime.c

%patch0 -p1

%build
cd src ; CFLAGS="$RPM_OPT_FLAGS -Wno-strict-aliasing" make %{?_smp_mflags} \
         PREFIX="%{_prefix}" LIBDIRNAME="/%{_lib}" all

%check
make %{?_smp_mflags} -C test all

%install
make PREFIX="%{_prefix}" DESTDIR=%{buildroot} LIBDIRNAME="/%{_lib}" install
rm -r %{buildroot}/%{_docdir}/faketime

%files
%_bindir/faketime
%_libdir/libfaketime*.so.*
%doc README COPYING NEWS README README.developers
%_mandir/man1/*

%changelog
* Mon Oct 14 2013 Dhiru Kholia <dhiru@openwall.com> - 0.9.5-1
- updated the package and borrowed fixes from Paul Wouters
* Tue Oct 08 2013 Dhiru Kholia <dhiru@openwall.com> - 0.9.1-1
- initial version borrowed from rosalinux
