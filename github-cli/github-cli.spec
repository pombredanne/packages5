Name:           github-cli
Version:        1.0.0
Release:        5%{?dist}
Summary:        A command-line interface to the GitHub Issues API v2
License:        BSD
URL:            http://github-cli.readthedocs.org/
Source0:        http://pypi.python.org/packages/source/g/github-cli/%{name}-%{version}.tar.gz
BuildRequires:  python2-devel
BuildRequires:  python-setuptools
BuildArch:      noarch

%description
github-cli provides an executable called ghi, that can be used to access all of
GitHub's documented Issues API (v2) functionality from your command-line

%prep
%setup -q
sed -i '1d' src/github/__init__.py

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%{_bindir}/ghi
%dir %{python_sitelib}/github
%{python_sitelib}/github/*
%{python_sitelib}/github_cli-*

%changelog
* Sat Sep 07 2013 Dhiru Kholia <dhiru@openwall.com> - 1.0.0-5
- started "co-maintaining" this package

* Sun Oct 21 2012 Adrian Alves <alvesadrian@fedoraproject.org> - 1.0.0-4
- Fixed %%setup and %%files

* Wed May 30 2012 Adrian Alves <alvesadrian@fedoraproject.org> - 1.0.0-3
- Fixed the source tag and the upstream source url

* Tue May 22 2012 Adrian Alves <alvesadrian@fedoraproject.org> - 1.0.0-2
- Added noarch tag
- Added how the fetch the source from git

* Mon May 21 2012 Adrian Alves <alvesadrian@fedoraproject.org> - 1.0.0-1
- First Build for Fedoraproject.org
