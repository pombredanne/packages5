# spec file for package gns3
#
# Copyright (c) 2013 SUSE LINUX Products GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
# https://build.opensuse.org/package/view_file/openSUSE:Factory/gns3/gns3.spec

Name:           gns3
Summary:        A graphical network simulator
License:        GPLv2
Version:        0.8.6
Release:        1
Url:            http://www.gns3.net/
Source0:        http://download.sf.net/gns-3/GNS3-%{version}-src.tar.gz
Source1:        %{name}.png
Source3:        %{name}.xml
Source4:        %{name}.desktop
Source5:        application-x-%{name}.png
BuildRequires:  desktop-file-utils
BuildRequires:  dos2unix
BuildRequires:  hicolor-icon-theme
BuildRequires:  libosip2-devel
BuildRequires:  python-devel
BuildRequires:  PyQt4-devel
BuildRequires:  sip-devel
BuildRequires:  shared-mime-info
Requires:       dynamips
Requires:       python-qt4
Requires:       sip
Provides:       GNS3 = %version
Obsoletes:      GNS3 < %version
BuildArch:      noarch

%description
GNS3 is a excellent complementary tool to real labs for administrators of Cisco
networks or people wanting to pass their CCNA, CCNP, CCIP or CCIE
certifications.

It can also be used to experiment features of Cisco IOS or to check
configurations that need to be deployed later on real routers.

Important notice: users must provide their own Cisco IOS to use GNS3.

%prep
%setup -q -n gns3-legacy-GNS3-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install --root $RPM_BUILD_ROOT --prefix /usr
%__mkdir_p %{buildroot}/%_datadir/%{name}
%__mkdir_p %{buildroot}/%_datadir/%{name}/examples
%__rm qemuwrapper/*.bat
%__cp -R qemuwrapper %{buildroot}/%_datadir/%{name}/
%__mkdir_p %{buildroot}/%_datadir/icons/hicolor/48x48/mimetypes/
%__mkdir_p %{buildroot}/%_datadir/mime/packages/
%__mkdir_p %{buildroot}/%_datadir/applications/
%__mkdir_p %{buildroot}/%_datadir/pixmaps
%__mkdir_p %{buildroot}/%_mandir/man1/
%__mkdir_p %{buildroot}/%_libexecdir/%name
%__mv qemuwrapper/qemuwrapper.py %{buildroot}/%{_libexecdir}/%{name}/qemuwrapper.py
%__mv vboxwrapper/vboxcontroller_4_1.py %{buildroot}/%_libexecdir/%{name}/vboxcontroller_4_1.py
%__mv vboxwrapper/vboxwrapper.py %{buildroot}/%_libexecdir/%{name}/vboxwrapper.py
%__mv %{buildroot}/usr/local/share/examples/gns3/baseconfig.txt %buildroot/%_datadir/%{name}/examples/
%__mv %{buildroot}/usr/local/share/examples/gns3/baseconfig_sw.txt %buildroot/%_datadir/%{name}/examples/
%__cp %SOURCE1 %{buildroot}/%_datadir/pixmaps
%__cp %SOURCE3 %{buildroot}/%_datadir/mime/packages/%{name}.xml
%__cp %SOURCE4 %{buildroot}/%_datadir/applications/
%__cp %SOURCE5 %{buildroot}/%_datadir/icons/hicolor/48x48/mimetypes/application-x-%{name}.png
%__cp docs/man/gns3.1 %{buildroot}/%_mandir/man1/
%__rm -rf %{buildroot}/usr/local/share/*
chmod -x CHANGELOG README TODO AUTHORS
chmod -x %{buildroot}%{_mandir}/man1/*
chmod -x %{buildroot}%{_datadir}/%{name}/examples/*
dos2unix README

%post
/usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :
/usr/bin/update-desktop-database

%postun
/usr/bin/update-mime-database %{_datadir}/mime &> /dev/null || :
/usr/bin/update-desktop-database

%files
%defattr(-, root, root, 0755)
%doc CHANGELOG README TODO AUTHORS
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/%{name}
%{_datadir}/icons/hicolor/48x48/mimetypes/application-x-%{name}.png
%{_datadir}/mime/packages/%{name}.xml
%{_mandir}/man1/*
%dir %_libexecdir/%{name}
%_libexecdir/%{name}/*
%dir %{python_sitelib}/GNS3
%{python_sitelib}/GNS3/*
%{python_sitelib}/GNS3*.egg-info

%changelog
* Sun Dec 22 2013 Dhiru Kholia <dhiru@openwall.com> - 0.2.10-1
- initial package (based on opensuse's .spec file)
