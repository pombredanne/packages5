%global gitrev 8f5212c

# this .spec is based on http://pkgs.fedoraproject.org/cgit/john.git/tree/john.spec

# upstream follows a different versioning scheme
# e.g. john-1.7.9-jumbo-7, john-1.8.0-jumbo-1
# we call "john-1.8.0-jumbo-1" as "john-jumbo-1.8.0.1"

Summary:          John the Ripper with jumbo patches
Name:             john-jumbo
Version:          1.8.0.1
Release:          1.git%{gitrev}%{?dist}

URL:              https://github.com/magnumripper/JohnTheRipper/tree/bleeding-jumbo
License:          GPLv2
Group:            Applications/System
# git clone https://github.com/magnumripper/JohnTheRipper/tree/bleeding-jumbo -b bleeding-jumbo john-jumbo-1.8.0 && pushd john-jumbo-1.8.0.1; git checkout 8f5212c; popd && tar -cjf john-jumbo-8f5212c.tar.bz2 john-jumbo-1.8.0.1/README* john-jumbo-1.8.0.1/run john-jumbo-1.8.0.1/src john-jumbo-1.8.0.1/doc
Source0:          john-jumbo-%{gitrev}.tar.bz2

%description
John the Ripper is a fast password cracker. Its primary purpose is to detect
weak Unix passwords, but a number of other hash types are supported as well.

The jumbo patch, which has been applied to this source tree of John the Ripper,
adds a lot of code, documentation, and data contributed by the user community.
This is not "official" John the Ripper code. It is very easy for new code to be
added to the jumbo patch: the quality requirements are low. This means that you
get a lot of functionality that is not "mature" enough or is otherwise
inappropriate for the official JtR, which in turn also means that bugs in this
code are to be expected, etc.

%prep
%setup -q
chmod 0644 doc/*
sed -i 's#\$JOHN/john.conf#%{_sysconfdir}/john.conf#' src/params.h
# cp -a src src-mmx

%build

%global target_non_mmx generic

%ifarch %{ix86}
%global target_non_mmx linux-x86-any
%global target_mmx linux-x86-mmx
%endif
%ifarch x86_64
%global target_non_mmx linux-x86-64
%global target_mmx linux-x86-64
%endif
%ifarch ppc
%global target_non_mmx linux-ppc32
%endif
%ifarch ppc64
%global target_non_mmx linux-ppc64
%endif

# export CFLAGS="-c ${RPM_OPT_FLAGS} -DJOHN_SYSTEMWIDE=1"

make -C src %{target_mmx} -j8

# CFLAGS="${CFLAGS} -DCPU_FALLBACK=1"
# LDFLAGS="${CFLAGS}"

# make -C src-mmx %{target_mmx} -j8
# %endif

%install
rm -rf %{buildroot}
install -d -m 755 %{buildroot}%{_sysconfdir}
install -d -m 755 %{buildroot}%{_bindir}
install -d -m 755 %{buildroot}%{_datadir}/john
install -m 755 run/{john,mailer} %{buildroot}%{_bindir}
install -m 644 run/{*.chr,password.lst} %{buildroot}%{_datadir}/john
install -m 644 run/john.conf %{buildroot}%{_sysconfdir}
# %if 0%{?target_mmx:1}
# install -d -m 755 %{buildroot}%{_libexecdir}/john
# install -m 755 run/john-non-mmx %{buildroot}%{_libexecdir}/john/
# %endif
pushd %{buildroot}%{_bindir}
ln -s john unafs
ln -s john unique
ln -s john unshadow
popd
rm doc/INSTALL

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc doc/*
%config(noreplace) %{_sysconfdir}/john.conf
%{_bindir}/john
%{_bindir}/mailer
%{_bindir}/unafs
%{_bindir}/unique
%{_bindir}/unshadow
%{_datadir}/john/

%changelog
* Sat Sep 07 2013 Dhiru Kholia <dhiru@openwall.com> - 1.8.0-0
- initial version
