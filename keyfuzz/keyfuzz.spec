%define name    keyfuzz
%define version 0.2

Name:            %{name}
Summary:         Keycode translator for multimedia keyboards
Version:         %{version}
Release:         1%{?dist}

Source:          http://0pointer.de/lennart/projects/keyfuzz/keyfuzz-0.2.tar.gz
URL:             http://0pointer.de/lennart/projects/keyfuzz/
License:         GPL
BuildRequires:   ImageMagick
BuildRequires:   lynx

%description
You may use keyfuzz to manipulate the scancode/keycode translation tables of
keyboard drivers supporting the Linux input layer API (as included in Linux
2.6). This is useful for fixing the translation tables of multimedia keyboards
or laptop keyboards with special keys. keyfuzz is not a daemon like Gnome acme
which reacts on special hotkeys but a tool to make non-standard keyboards
compatible with such daemons. keyfuzz should be run once at boot time, the
modifications it makes stay active after the tool quits until reboot. keyfuzz
does not interact directly with XFree86. However, newer releases of the latter
(4.1 and above) rely on the Linux input API, so they take advantage of the
fixed translation tables.

%prep
%setup -q

%build
%configure
make

%install
make install DESTDIR=%{buildroot}

%files
%defattr(-,root,root)
%doc README doc/*.html doc/*.css
%{_sbindir}/%name
%{_mandir}/man8/*
%{_sysconfdir}/init.d/*
%{_sysconfdir}/%name

%changelog
* Wed Sep 25 2013 Dhiru Kholia <dhiru@openwall.com> - 0.2-1
- initial package (based on rosalinux .spec file)
- patches borrowed from Arch Linux
