%if 0%{?fedora}
%global with_python3 1
%endif

%global upstream_name pudb
%global module_name pudb

Name:          python-pudb
Version:       2013.5.1
Release:       1%{?dist}
Summary:       A full-screen, console-based Python debugger
License:       MIT
URL:           http://mathema.tician.de/software/pudb
Source0:       http://pypi.python.org/packages/source/p/%{upstream_name}/%{upstream_name}-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: python2-devel
BuildRequires: python-setuptools
Requires:      python-urwid
Requires:      python-pygments

%description
PuDB is a full-screen, console-based visual debugger for Python.

Its goal is to provide all the niceties of modern GUI-based debuggers in a more
lightweight and keyboard-friendly package. PuDB allows you to debug code right
where you write and test it--in a terminal. If you've worked with the excellent
(but nowadays ancient) DOS-based Turbo Pascal or C tools, PuDB's UI might look
familiar.

%if 0%{?with_python3}
%package -n python3-%{upstream_name}
Summary:       A full-screen, console-based Python debugger

BuildRequires: python3-devel
BuildRequires: python3-setuptools
Requires:      python3-urwid
Requires:      python3-pygments

%description -n python3-%{upstream_name}
PuDB is a full-screen, console-based visual debugger for Python.

Its goal is to provide all the niceties of modern GUI-based debuggers in a more
lightweight and keyboard-friendly package. PuDB allows you to debug code right
where you write and test it--in a terminal. If you've worked with the excellent
(but nowadays ancient) DOS-based Turbo Pascal or C tools, PuDB's UI might look
familiar.
%endif # if with_python3

%prep
%setup -q -n %{upstream_name}-%{version}
rm -rf %{module_name}.egg-info

sed -i '1{\@^#! /usr/bin/env python@d}' pudb/debugger.py

%if %{with python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build
%{__python2} setup.py build

%if %{with python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%if %{with python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif

%files
%doc README.rst LICENSE
%{python2_sitelib}/%{module_name}
%{python2_sitelib}/%{module_name}*.egg-info
%{_bindir}/pudb

%if %{with python3}
%files -n python3-%{upstream_name}
%doc README.rst LICENSE
%{python3_sitelib}/%{module_name}
%{python3_sitelib}/%{module_name}*.egg-info
%{_bindir}/pudb3
%endif

%changelog
* Mon Dec 09 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.5.1-1
- update to pudb-2013.5.1

* Wed Nov 13 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.4-1
- update to pudb-2013.4

* Tue Sep 10 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.3.6-2
- replaced {python_sitelib} with {python2_sitelib}

* Fri Sep 06 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.3.6-1
- update to upstream release 2013.3.6

* Fri Sep 06 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.3.5-5
- replaced {__python} with {__python2}

* Thu Sep 05 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.3.5-4
- make layout of install section consistent with that of build section

* Thu Sep 05 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.3.5-3
- removed the unnecessary attr directive
- fixed the location of the sed command invocation

* Thu Sep 05 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.3.5-2
- implemented initial fixes suggested in BZ #1004257
- removed unused EL5 support, removed invalid BuildRoot, fixed BuildRequires

* Wed Sep 04 2013 Dhiru Kholia <dhiru@openwall.com> - 2013.3.5-1
- initial version
