%global module_name backports.lzma

Name:          python-backports-lzma
Version:       0.0.2
Release:       1%{?dist}
Summary:       Backport of Python 3.3's 'lzma' module for XZ/LZMA compressed files
License:       BSD
URL:           https://github.com/peterjc/backports.lzma
Source0:       http://pypi.python.org/packages/source/b/%{module_name}/%{module_name}-%{version}.tar.gz

BuildRequires: python2-devel
BuildRequires: python-setuptools
BuildRequires: xz-devel
Requires:      python-backports
Requires:      xz-libs%{?_isa}

%description
Python 3.3 onwards includes in the standard library module 'lzma', providing
support for working with LZMA and XZ compressed files via the XZ Utils C
library (XZ Utils is in a sense LZMA v2). See:

    Python's lzma - http://docs.python.org/dev/library/lzma.html
    XZ Utils - http://tukaani.org/xz/

This code is a backport of the Python 3.3 standard library module 'lzma' for
use on older versions of Python where it was not included. It is available from
PyPI (released downloads only) and GitHub (repository):

    PyPI - http://pypi.python.org/pypi/backports.lzma/
    GitHub - https://github.com/peterjc/backports.lzma

There are some older Python libraries like PylibLZMA and PyLZMA but these are
both using LZMA Utils (not XZ Utils, so they have no XZ support).

    PylibLZMA - http://pypi.python.org/pypi/pyliblzma
    PyLZMA - http://pypi.python.org/pypi/pylzma/
    LZMA Utils - http://tukaani.org/lzma/

%prep
%setup -qn %{module_name}-%{version}

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}
rm %{buildroot}%{python_sitearch}/backports/__init__.py*

%files
%{python_sitearch}/*

%changelog
* Mon Sep 09 2013 Dhiru Kholia <dhiru@openwall.com> - 0.0.2-1
- initial version
