try:
    import lzma
except ImportError:
    from backports import lzma

# then use lzma as normal, for example:
assert b"Hello!" == lzma.decompress(lzma.compress(b"Hello!"))
