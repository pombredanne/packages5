%global upstream_name beanstalkc
%global upstream_version 0.3.0

name:          python-%{upstream_name}
Version:       %{upstream_version}
Release:       2%{?dist}
Summary:       A simple beanstalkd client library for Python
License:       ASL 2.0
URL:           https://github.com/earl/beanstalkc
Source0:       http://pypi.python.org/packages/source/b/%{upstream_name}/%{upstream_name}-%{upstream_version}.tar.gz

BuildRequires: python2-devel
BuildRequires: python-setuptools
BuildArch:     noarch

%description
beanstalkc is a simple beanstalkd client library for Python. beanstalkd is a
fast, distributed, in-memory workqueue service.

%prep
%setup -qn %{upstream_name}-%{upstream_version}
rm -rf %{upstream_name}.egg-info
sed -i -e '/^#!\//, 1d' beanstalkc.py

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%{python2_sitelib}/*

%changelog
* Tue Sep 17 2013 Dhiru Kholia <dhiru@openwall.com> - 0.3.0-2
- remove shebang from beanstalkc.py

* Sun Sep 15 2013 Dhiru Kholia <dhiru@openwall.com> - 0.3.0-1
- initial version
