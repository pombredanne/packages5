%global baselib libarchive

# upstream uses a weird versioning (e.g. 3.1.2-1) scheme and versions have
# characters not allowed in RPM's Version field.

%global upstream_version 3.1.2
%global upstream_release 1

name:          python-%{baselib}
# "Version" is upstream_version + upstream_release
# "-" has been replaced by "." as suggested by #fedora-devel folks
Version:       %{upstream_version}.%{upstream_release}
Release:       3%{?dist}
Summary:       Fully functional Python wrapper for libarchive
License:       BSD
URL:           http://code.google.com/p/python-libarchive/
Source0:       http://pypi.python.org/packages/source/p/%{name}/%{name}-%{upstream_version}-%{upstream_release}.tar.gz

BuildRequires: python2-devel
BuildRequires: python-setuptools
BuildRequires: %{baselib}-devel = %{upstream_version}
BuildRequires: swig
Requires:      %{baselib}%{?_isa} = %{upstream_version}

%description
Fully functional Python wrapper for libarchive. Created using SWIG. The
low-level API provides access to all of the functionality of libarchive.

The package also includes a high level python library which mimics the zipfile
and tarfile modules.

Libarchive supports the following:

- Reads a variety of formats, including tar, pax, cpio, zip, xar, lha, ar, cab,
  mtree, rar, and ISO images.

- Writes tar, pax, cpio, zip, xar, ar, ISO, mtree, and shar archives.

- Automatically handles archives compressed with gzip, bzip2, lzip, xz, lzma,
  or compress.

%prep
%setup -qn %{name}-%{upstream_version}-%{upstream_release}
rm -rf %{baselib}.egg-info

%build
%{__python} setup.py build

%install
%{__python} setup.py install --skip-build --root %{buildroot}
%{_fixperms} %{buildroot}

%check
pushd %{baselib}
make
popd
%{__python} tests.py

%files
%doc README.rst
%{python_sitearch}/*

%changelog
* Sat Sep 07 2013 Dhiru Kholia <dhiru@openwall.com> - 3.1.2.1-3
- fix non-standard-executable-perms reported in BZ #1004845

* Thu Sep 05 2013 Dhiru Kholia <dhiru@openwall.com> - 3.1.2.1-2
- fix build problems reported in BZ #1004845

* Thu Sep 05 2013 Dhiru Kholia <dhiru@openwall.com> - 3.1.2.1-1
- initial version
