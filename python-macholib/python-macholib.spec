%global upstream_name macholib

Name:          python-macholib
Version:       1.5.1
Release:       1%{?dist}
Summary:       Mach-O header analysis and editing
License:       MIT
URL:           http://pythonhosted.org/macholib/
Source0:       http://pypi.python.org/packages/source/m/%{upstream_name}/%{upstream_name}-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: python2-devel
BuildRequires: python-setuptools
BuildRequires: python-sphinx
Requires:      python-altgraph

%description
macholib can be used to analyze and edit Mach-O headers, the executable format
used by Mac OS X.

It's typically used as a dependency analysis tool, and also to rewrite dylib
references in Mach-O headers to be @executable_path relative.

Though this tool targets a platform specific file format, it is pure python
code that is platform and endian independent.

%prep
%setup -q -n %{upstream_name}-%{version}
rm -rf %{upstream_name}.egg-info

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%doc README.txt doc/license.rst
%{python2_sitelib}/%{upstream_name}
%{python2_sitelib}/%{upstream_name}*.egg-info
%{_bindir}/macho_dump
%{_bindir}/macho_find
%{_bindir}/macho_standalone

%changelog
* Mon Oct 21 2013 Dhiru Kholia <dhiru@openwall.com> - 1.5.1-1
- initial version
