%global upstream_name mitmproxy
%global module_name mitmproxy

Name:          python-%{upstream_name}
Version:       0.10
Release:       3%{?dist}
Summary:       An interactive SSL-capable intercepting HTTP proxy

License:       MIT
URL:           http://www.mitmproxy.org/
Source0:       http://pypi.python.org/packages/source/m/%{upstream_name}/%{upstream_name}-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: python-setuptools
BuildRequires: python2-devel

Requires:      python-netlib
Requires:      python-flask
Requires:      python-urwid

%description
mitmproxy is an interactive, SSL-capable man-in-the-middle proxy for HTTP with
a console interface.

mitmdump is the command-line version of mitmproxy. Think tcpdump for HTTP.

libmproxy is the library that mitmproxy and mitmdump are built on.

%prep
%setup -q -n %{upstream_name}-%{version}
rm -rf %{module_name}.egg-info

sed -i '1{\@^#!/usr/bin/env python@d}' libmproxy/contrib/html2text.py

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%doc README.txt LICENSE
%{python2_sitelib}/*
%{_bindir}/*

%changelog
* Wed Mar 12 2014 Dhiru Kholia <dhiru@openwall.com> - 0.10-3
- fixed non-executable-script error (BZ #1070418)

* Thu Feb 27 2014 Dhiru Kholia <dhiru@openwall.com> - 0.10-2
- changed URL, fixed description, fixed doc section (BZ #1070418)

* Thu Feb 27 2014 Dhiru Kholia <dhiru@openwall.com> - 0.10-1
- initial version
