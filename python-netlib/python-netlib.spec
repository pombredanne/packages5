%global upstream_name netlib
%global module_name netlib

Name:          python-%{upstream_name}
Version:       0.10
Release:       3%{?dist}
Summary:       Collection of network utility classes

License:       MIT
URL:           https://github.com/mitmproxy/netlib
Source0:       http://pypi.python.org/packages/source/n/%{upstream_name}/%{upstream_name}-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: python-setuptools
BuildRequires: python2-devel
Requires:      python-pyasn1 pyOpenSSL

%description
Netlib is a collection of network utility classes, used by the pathod and
mitmproxy projects. It differs from other projects in some fundamental
respects, because both pathod and mitmproxy often need to violate standards.
This means that protocols are implemented as small, well-contained and flexible
functions, and are designed to allow misbehaviour when needed.

%prep
%setup -q -n %{upstream_name}-%{version}
rm -rf %{module_name}.egg-info

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%doc README.mkd
%{python2_sitelib}/*

%changelog
* Thu Feb 27 2014 Dhiru Kholia <dhiru@openwall.com> - 0.10-3
- removed group tag, own files only (BZ #1069124)

* Tue Feb 25 2014 Dhiru Kholia <dhiru@openwall.com> - 0.10-2
- fix build problems

* Mon Feb 24 2014 Dhiru Kholia <dhiru@openwall.com> - 0.10-1
- initial version
