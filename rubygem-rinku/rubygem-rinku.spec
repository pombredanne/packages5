%global gem_name rinku

Summary: Auto-linking. Ruby. Yes, that's pretty much it
Name: rubygem-%{gem_name}
Version: 1.7.3
Release: 1%{?dist}
Group: Development/Languages
License: MIT
URL: https://github.com/vmg/rinku
Source0: http://rubygems.org/downloads/%{gem_name}-%{version}.gem
Requires: ruby(rubygems)
Requires: ruby(release)
Requires: rubygem(rinku)
BuildRequires: rubygems-devel
BuildRequires: ruby-devel
Provides: rubygem(%{gem_name}) = %{version}

%description
A fast and very smart auto-linking library that acts as a drop-in replacement
for Rails auto_link.

%prep
gem unpack %{SOURCE0}
%setup -q -D -T -n  %{gem_name}-%{version}
gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
export CONFIGURE_ARGS="$CONFIGURE_ARGS --with-pkg-config-dir=$(pwd)%{_libdir}/pkgconfig"
gem build %{gem_name}.gemspec
%gem_install

find . -name \*.gem | xargs chmod 0644

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
	%{buildroot}%{gem_dir}/
pushd %{buildroot}
# move C extension files to the correct directory
mkdir -p .%{gem_extdir_mri}/lib
mv .%{gem_instdir}/lib/%{gem_name}.so .%{gem_extdir_mri}/lib
popd

%check

%files
%dir %{gem_instdir}
%dir %{gem_instdir}/lib/
%{gem_extdir_mri}/lib/%{gem_name}.so
%{gem_libdir}
%{gem_cache}
%{gem_spec}
%doc %{gem_docdir}
%{gem_instdir}/test
%attr(644,-,-) %{gem_instdir}/COPYING
%{gem_instdir}/README.markdown
%{gem_instdir}/Rakefile
%exclude %{gem_instdir}/ext
%exclude %{gem_instdir}/ri
%exclude %{gem_instdir}/rinku.gemspec

%changelog
* Thu Feb 06 2014 Dhiru Kholia <dhiru@openwall.com> - 1.7.3-1
- Initial package (largely stolen from Mamoru and Kevin)
