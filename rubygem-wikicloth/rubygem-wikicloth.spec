%global gem_name wikicloth

Summary: Ruby implementation of the MediaWiki markup language
Name: rubygem-%{gem_name}
Version: 0.8.1
Release: 1%{?dist}
Group: Development/Languages
License: MIT
URL: https://github.com/nricciar/wikicloth
Source0: http://rubygems.org/downloads/%{gem_name}-%{version}.gem
Requires: ruby(rubygems)
Requires: ruby(release)
Requires: rubygem(simplecov)
Requires: rubygem(activesupport)
Requires: rubygem(rdoc)
Requires: rubygem(rake)
Requires: rubygem(i18n)
Requires: rubygem(builder)
Requires: rubygem(expression_parser)
Requires: rubygem(rinku)
Requires: rubygem(test-unit)
BuildRequires: rubygems-devel
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Ruby implementation of the MediaWiki markup language.

%prep

%build

%install
%gem_install -n %{SOURCE0} -d %{buildroot}%{gem_dir}

%check

%files
%dir %{gem_instdir}
%{gem_libdir}
%{gem_cache}
%{gem_spec}
%{gem_instdir}/test
%{gem_instdir}/Rakefile
%{gem_instdir}/README
%{gem_instdir}/MIT-LICENSE
%doc %{gem_docdir}
%exclude %{gem_instdir}/.gitignore
%exclude %{gem_instdir}/Gemfile
%exclude %{gem_instdir}/wikicloth.gemspec
%exclude %{gem_instdir}/sample_documents
%exclude %{gem_instdir}/lang
%exclude %{gem_instdir}/examples
%exclude %{gem_instdir}/init.rb
%exclude %{gem_instdir}/.travis.yml
%exclude %{gem_instdir}/README.textile
%exclude %{gem_instdir}/run_tests.rb
%exclude %{gem_instdir}/tasks

%changelog
* Thu Feb 06 2014 Dhiru Kholia <dhiru@openwall.com> - 0.8.1-1
- Initial package
