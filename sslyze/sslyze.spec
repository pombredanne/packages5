%global name sslyze

%global upstream_version 0.7
%global upstream_version_wtf 0_7

name:          sslyze
Version:       %{upstream_version}
Release:       1%{?dist}
Summary:       Fast and full-featured SSL scanner.
License:       GPL
URL:           https://github.com/iSECPartners/sslyze
Source0:       https://www.dropbox.com/s/zf0e8oolkpkcuhu/%{name}-%{upstream_version_wtf}-linux64.zip

BuildRequires: python2-devel
BuildRequires: python-setuptools

%description
SSLyze is a Python tool that can analyze the SSL configuration of a server by
connecting to it. It is designed to be fast and comprehensive, and should help
organizations and testers identify misconfigurations affecting their SSL
servers.

Key features include:

 - Multi-processed and multi-threaded scanning (it's fast)
 - SSL 2.0/3.0 and TLS 1.0/1.1/1.2 compatibility
 - Performance testing: session resumption and TLS tickets support
 - Security testing: weak cipher suites, insecure renegotiation, CRIME and more
 - Server certificate validation and revocation checking through OCSP stapling
 - Support for StartTLS handshakes on SMTP, XMPP, LDAP, POP, IMAP and FTP
 - Support for client certificates when scanning servers that perform mutual authentication
 - XML output to further process the scan results
 - And much more !

%prep
# rm -rf __MACOSX
%setup -q -n %{name}-%{upstream_version_wtf}-linux64

%build
%{__python} setup.py build

%install
%{__python} setup.py install --skip-build --root %{buildroot}

%files
%{python_sitelib}/*
%{_bindir}/sslyze.py

%changelog
* Fri Sep 06 2013 Dhiru Kholia <dhiru@openwall.com> - 0.7-1
- initial version

