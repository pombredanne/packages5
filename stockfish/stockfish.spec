%global upstream_name Stockfish
%global upstream_short_name sf
%global book_version 231

Name:           stockfish
Version:        4
Release:        2%{dist}
Summary:        Powerful open source chess engine
License:        GPLv3
URL:            http://stockfishchess.org/
Source0:        https://github.com/mcostalba/Stockfish/archive/%{upstream_short_name}_%{version}.tar.gz
# Stockfish opening book can be downloaded from http://stockfishchess.org/download/
# $ sha256sum stockfish-231-book.zip
# 07ee06bcd3fe1c5e0d90a2d0aede8bac2dd31d52fb4e1c9bc477bb5a8939e414  stockfish-231-book.zip
Source1:        %{name}-%{book_version}-book.zip
Patch0:         %{name}-%{version}-mga-adjust-opening-book.patch
ExclusiveArch:  %{ix86} x86_64 armv7hl

%description
Stockfish is a free UCI chess engine derived from Glaurung 2.1. It is not a
complete chess program, but requires some UCI compatible GUI (like XBoard with
PolyGlot, eboard, Arena, Sigma Chess, Shredder, Chess Partner or Fritz) in
order to be used comfortably. Read the documentation for your GUI of choice for
information about how to use Stockfish with your GUI.

%prep
%setup -q -a 1 -n %{upstream_name}-%{upstream_short_name}_%{version}

%patch0 -p0

# change the default book path to a system-wide location
# http://fedoraproject.org/wiki/SIGs/Games/Packaging
# UCI command to use book from CLI is "setoption name OwnBook value true"
sed -i "/Book File/s:book.bin:/usr/share/stockfish/Book.bin:" src/ucioption.cpp
sed -i "s/debug = no/debug = yes/" src/Makefile

%build
pushd src
%ifarch x86_64
  make build ARCH=x86-64 %{?_smp_mflags}
%else
  %ifarch %{ix86}
    make build ARCH=x86-32 %{?_smp_mflags}
  %else
    %ifarch armv7hl
       make build ARCH=armv7 %{?_smp_mflags}
    %endif
  %endif
%endif
popd

%install

mkdir -p $RPM_BUILD_ROOT%{_var}/games/%{name} $RPM_BUILD_ROOT%{_bindir} $RPM_BUILD_ROOT%{_datadir}/%{name}
install -m 755 -p src/%{name} $RPM_BUILD_ROOT%{_bindir}

# the opening book
install -m 644 -p Book.bin $RPM_BUILD_ROOT%{_datadir}/%{name}

# polyglot file (runtime configuration file)
install -m 644 -p polyglot.ini $RPM_BUILD_ROOT%{_var}/games/%{name}

%files
%doc Readme.md Copying.txt
%{_datadir}/%{name}
%{_var}/games/%{name}
%{_bindir}/%{name}

%changelog
* Tue Sep 10 2013 Dhiru Kholia <dhiru@openwall.com> - 4-2
- fixed prep section and book path, removed dos2unix call, confirm to FHS
- preserve timestamps for resources, use ExclusiveArch, preserve debug symbols

* Tue Sep 10 2013 Dhiru Kholia <dhiru@openwall.com> - 4-1
- initial version based on stockfish.spec from mageia
