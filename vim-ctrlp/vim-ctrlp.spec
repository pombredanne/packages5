%global vimfiles_root %{_datadir}/vim/vimfiles

%global upstream_name ctrlp.vim

Name:           vim-ctrlp
Version:        1.79
Release:        1%{?dist}
Summary:        Full path fuzzy file, buffer, mru, tag, ... finder for Vim

License:        Vim
URL:            http://kien.github.io/ctrlp.vim/

# Upstream releases tarballs at https://github.com/kien/ctrlp.vim/releases
# However accessing https://github.com/kien/ctrlp.vim/archive/1.79.tar.gz
# downloads ctrlp.vim-1.79.tar.gz file *and* this is problematic.
Source0:        %{upstream_name}-%{version}.tar.gz

Requires:       vim-common
Requires(post): vim
Requires(postun): vim
BuildArch:      noarch

%description
Full path fuzzy file, buffer, mru, tag, ... finder for Vim.

 - Written in pure Vimscript for MacVim, gVim and Vim 7.0+.
 - Full support for Vim's regexp as search patterns.
 - Built-in Most Recently Used (MRU) files monitoring.
 - Built-in project's root finder.
 - Open multiple files at once.
 - Create new files and directories.
 - Extensible.

%prep
%setup -q -n %{upstream_name}-%{version}

%build

%install
mkdir -p %{buildroot}%{vimfiles_root}
cp -ar {autoload,doc,plugin} %{buildroot}%{vimfiles_root}

%post
vim -c ":helptags %{vimfiles_root}/doc" -c :q &> /dev/null

%postun
rm %{vimfiles_root}/doc/tags
vim -c ":helptags %{vimfiles_root}/doc" -c :q &> /dev/null


%files
%doc readme.md
%doc %{vimfiles_root}/doc/*
%{vimfiles_root}/autoload
%{vimfiles_root}/plugin/*

%changelog
* Sun Sep 08 2013 Dhiru Kholia <dhiru@openwall.com> - 1.79-1
- initial version
